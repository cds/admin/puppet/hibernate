class hibernate (
   String $hibernate_target_path, 
   String $sleep_target_path, 
   String $suspend_target_path, 
   String $hybrid_sleep_target_path, 
) { 
    if $facts['os']['family'] == 'RedHat' {
	file  { 'hibernate_target':
	    path => $hibernate_target_path,
	    ensure => link,
	    target => '/dev/null',
	    seluser => "system_u",
	    selrole => "object_r",
	    seltype => "systemd_unit_file_t",
	}
	file  { 'sleep_target':
	    path => $sleep_target_path,
	    ensure => link,
	    target => '/dev/null',
	    seluser => "system_u",
	    selrole => "object_r",
	    seltype => "systemd_unit_file_t",
	}
	file  { 'suspend_target':
	    path => $suspend_target_path,
	    ensure => link,
	    target => '/dev/null',
	    seluser => "system_u",
	    selrole => "object_r",
	    seltype => "systemd_unit_file_t",
	}
	file  { 'hybrid_sleep_target':
	    path => $hybrid_sleep_target_path,
	    ensure => link,
	    target => '/dev/null',
	    seluser => "system_u",
	    selrole => "object_r",
	    seltype => "systemd_unit_file_t",
	}
    }
}
